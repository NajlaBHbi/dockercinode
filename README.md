# Docker-CircleCi-Node-Angular
--------------------------------------
 
Docker images for angular cli app.

[![CircleCI](https://circleci.com/bb/NajlaBHbi/dockercinode.svg?style=svg)](https://circleci.com/bb/NajlaBHbi/dockercinode)


## About
==========

* Test CircleCi for angular app.


	
## Usage
==========

docker pull bhntools/circlenode:latest


## Authors & Maintainer
====================

BEN HASSINE Najla.



## License
========

MIT License.

